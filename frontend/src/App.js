import React, { Suspense } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { Container } from 'react-bootstrap';
import Home from "./Home";
import Navigation from "./Navigation";
import Activities from "./Activities";
import Activity from "./Activity";
import Register from "./Register";
import Registration from "./Registration"
import NotFound from "./NotFound"
import './App.css';

function App() {
  return (
    <BrowserRouter basename={process.env.REACT_APP_BASENAME}>
    {/* Navbar mustn't disappear when loading a new page, because navbar language doesn't change with it. */}
    <Suspense fallback={<div />}>
      <Navigation />
    </Suspense>
    <Container>
        <Suspense fallback={<div>"Loading"</div>}>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/activities/:activityId/registrations/:registrationId" component={Registration}/>
            <Route path="/activities/:id/:date/register" component={Register}/>
            <Route path="/activities/:id" component={Activity}/>
            <Route path="/activities" component={Activities}/>
            <Route path="/404" component={NotFound}/>
          </Switch>
        </Suspense>
    </Container>
    <Suspense fallback={<div />}/>
  </BrowserRouter>
  );
}

export default App;
