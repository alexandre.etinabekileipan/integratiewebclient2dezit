import React from "react";

function Home(){
    return (<>
    <h1>Blended registration application</h1>
    <div>Welcome to the home page. Use the buttons in the top-left corner to navigate the page.</div>
    </>);
}

export default Home;