import { Button } from 'react-bootstrap';
import React from 'react';
import { withRouter } from 'react-router-dom';

function Navigation() {
    return (
        <>
            <Button href="/">Home</Button>
            <Button href="/activities">All activities</Button>
        </>
    );
}



export default withRouter(Navigation);

