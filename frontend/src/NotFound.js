import React from "react";

function NotFound(){
    return (
        <>
            <h1>Oops</h1>
            <p>The item you requested does not seem to exist. Go <a href="/">home?</a></p>
        </>
    );
}

export default NotFound;