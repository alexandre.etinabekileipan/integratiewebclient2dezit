import React from "react";
import * as yup from "yup";
import {BASE_URL} from "./Constants";
import {Formik} from "formik";
const registerSchema = 
  yup.object().shape({
    firstName: yup
      .string()
      .trim()
      .required("May not be empty"),
    lastName: yup
      .string()
      .trim()
      .required("May not be empty"),
    telephoneNumber: yup //not required but will be automatically copy shortname if not given
      .string()
      .trim()
      .required("May not be empty"),
    email: yup //not required but will be automatically copy shortname if not given
      .string()
      .email("Must be a valid email")
      .trim()
      .required("May not be empty"),
  });

class Register extends React.Component {

    constructor(props){
        super(props);
        const id = this.props.match.params.id;
        const date = this.props.match.params.date;
        this.state = {
            "firstName": "",
            "lastName": "",
            "telephoneNumber": "",
            "email": "",
            "activityId": id,
            "date": date
        }
    }

    submitForm(values){
        this.setState(values);
        const {activityId} = this.state;
        console.log(this.state);
        fetch(`${BASE_URL}/activities/${activityId}/register`, {
            'headers': {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            "method": "POST",
            "body": JSON.stringify(this.state),
        }).then(res => res.json()).then(res => {
            console.log(res);
            this.props.history.push(`/activities/${activityId}/registrations/${res.id}`)
        })
        .catch(() => this.props.history.push("/"));
    }

    render(){
        return (
            <Formik
              initialValues={this.state}
              onSubmit={(values) => this.submitForm(values)}
              validationSchema={registerSchema}
            >
              {(formik) => {
                const {
                  values,
                  handleChange,
                  handleSubmit,
                  errors,
                  touched,
                  handleBlur,
                  isValid,
                  dirty
                } = formik;
                return (
                    <div className="container">
                      <h1>Fill in your information to register</h1>
                      <form onSubmit={handleSubmit}>
                      <div className="form-row">
                          <label htmlFor="firstName">First name</label>
                          <input
                            type="text"
                            name="firstName"
                            id="firstName"
                            value={values.firstName}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.firstName && touched.firstName ? 
                            "input-error" : null}
                          />
                          {errors.firstName && touched.firstName && (
                            <span className="error">{errors.firstName}</span>
                          )}
                        </div>
                        <div className="form-row">
                          <label htmlFor="lastName">Last name</label>
                          <input
                            type="text"
                            name="lastName"
                            id="lastName"
                            value={values.lastName}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.lastName && touched.lastName ? 
                            "input-error" : null}
                          />
                          {errors.lastName && touched.lastName && (
                            <span className="error">{errors.lastName}</span>
                          )}
                        </div>
                        <div className="form-row">
                          <label htmlFor="telephoneNumber">Telephone number</label>
                          <input
                            type="text"
                            name="telephoneNumber"
                            id="telephoneNumber"
                            value={values.telephoneNumber}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.telephoneNumber && touched.telephoneNumber ? 
                            "input-error" : null}
                          />
                          {errors.telephoneNumber && touched.telephoneNumber && (
                            <span className="error">{errors.telephoneNumber}</span>
                          )}
                        </div>
                        <div className="form-row">
                          <label htmlFor="email">Email</label>
                          <input
                            type="email"
                            name="email"
                            id="email"
                            value={values.email}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            className={errors.email && touched.email ? 
                            "input-error" : null}
                          />
                          {errors.email && touched.email && (
                            <span className="error">{errors.email}</span>
                          )}
                        </div>
        
                        <button
                          type="submit"
                          className={dirty && isValid ? "" : "disabled-btn"}
                          disabled={!(dirty && isValid)}>
                          Register
                        </button>
                      </form>
                    </div>
                );
              }}
            </Formik>
        );
    }
}

export default Register;