import React from "react";
import {Table, Button} from "react-bootstrap";
import {BASE_URL} from "./Constants";

class Activity extends React.Component {
    state = {"firstName": "", "lastName": "", "telephoneNumber": "", "email": "", "cancelledDate": "",};

    constructor(props){
        super(props);
        this.handleRegistrationCancel = this.handleRegistrationCancel.bind(this);
    }

    componentDidMount(){
        const {activityId, registrationId} = this.props.match.params;
        fetch(`${BASE_URL}/activities/${activityId}/registrations/${registrationId}`)
        .then(res => res.json()).then(res => {
            if(res.status === 404){
                return this.props.history.push("/404")
            };
            return this.setState(res);
        })
    }

    handleRegistrationCancel(){
        const {activityId, registrationId} = this.props.match.params;
        fetch(`${BASE_URL}/activities/${activityId}/registrations/${registrationId}`, {
            "method": "DELETE"
        }).then(res => res.json())
        .then(res => this.setState(res));
    }

    render(){
        const {firstName, lastName, email, telephoneNumber, cancelledDate} = this.state;
        return (
            <>
            <Table striped bordered hover>
              <thead>
                  <tr>
                  <th scope="col">First name</th>
                  <th scope="col">Last name</th>
                  <th scope="col">Telephone number</th>
                  <th scope="col">Email</th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td>{firstName}</td>
                      <td>{lastName}</td>
                      <td>{telephoneNumber}</td>
                      <td>{email}</td>
                  </tr>
              </tbody>
          </Table>
          {cancelledDate ? `Registration cancelled on ${cancelledDate}`:
          <Button onClick={() => this.handleRegistrationCancel()}>Cancel registration</Button> }
            </>
        );
    }
}

export default Activity;