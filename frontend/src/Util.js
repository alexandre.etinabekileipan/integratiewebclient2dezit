export function toISODate(date) {
    if(date){
      return date.toISOString().split("T")[0];
    } else {
      return "";
    }
  }